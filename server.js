//1. 启动服务器
// 为静态网页创建打开方式 


// 2.书写接口
// 在网络上传数据的地址，目的的是与前端交互

let express = require('express')

let app = express();

let bodyParser = require('body-parser');

// 解析post请求的参数,解析出来放到req.body中
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
// let bodyParser =require('body-parser');

// // application/json
// let jsonParser = bodyParser.json();

// // // application/x-www-form-urlencoded
// let lencodedParser = bodyParser.urlencoded({extended: false})

//启用静态服务器
//为某一个文件夹启用服务器
//可以直接打开里面的文件

app.use(express.static(__dirname+'/static'))

app.use(express.static(__dirname+'/upload'))

let router = require('./router')
// 配置跨域请求中间件(服务端允许跨域请求)
app.all('*', function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', '*'),//可根据浏览器的F12查看,把对应的粘贴在这里就行
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    res.header('Access-Control-Allow-Methods', '*');
    res.header('Content-Type', 'application/json;charset=utf-8');
    next();
  });



//获取消息列表
app.get("/news",router.getNews).post('news',router.getNews)

app.get("/api/getcode",router.getCode).post('news',router.getCode)

//注册
app.post('/api/register',router.register)

//登录////
app.post("/api/login",router.login)

//验证登录
app.post("/verifylogin",router.verifyLogin)

//退出登录
app.post("/loginout",router.loginout)
//本机访问端口
app.listen(3000)
console.log('3000启动')