import Vue from 'vue'
import Router from 'vue-router'

// import Login from './components/Login.vue'
const Login = () => import('../components/Login.vue')
// import Home from './components/Home.vue'
const Home = () => import('../components/Home.vue')

const Welcome = () => import('../components/Welcome.vue')

Vue.use(Router)

const router = new Router({
  routes: [
    { path: '/', redirect: '/login' },
    { path: '/login', component: Login },
    {
      path: '/home',
      component: Home,
      redirect: '/home',
      children: [
        { path: '/home', component: Home }
        // { path: '/users', component: Users },
        // { path: '/rights', component: Rights },
        // { path: '/roles', component: Roles },
        // { path: '/categories', component: Cate },
        // { path: '/params', component: Params },
        // { path: '/goods', component: GoodsList },
        // { path: '/goods/add', component: Add },
        // { path: '/orders', component: Order },
        // { path: '/reports', component: Report }
      ]
    }
  ]
})

// 挂载路由导航守卫
router.beforeEach((to, from, next) => {
  // to 将要访问的路径
  // from 代表从哪个路径跳转而来
  // next 是一个函数，表示放行
  //     next()  放行    next('/login')  强制跳转

  if (to.path === '/login') return next()
  // 获取token
  const tokenStr = window.localStorage.getItem('exam')
  if (!tokenStr) return next('/login')
  next()
})

export default router
