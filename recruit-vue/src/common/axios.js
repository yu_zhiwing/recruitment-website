import axios from 'axios'

export function axiosRequest (config) {
  const allAxios = axios.create({
    baseURL: 'http://127.0.0.1:3000',
    timeout: 5000
  })
  return allAxios(config)
}
