// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css';
import {axiosRequest} from '@/Common/axios'

//公共样式
import './css/public.less'
// 配置请求的跟路径
Vue.config.productionTip = false
Vue.use(ElementUI)
Vue.prototype.axiosRequest = axiosRequest



/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
