//注册接口
let url = require('url')
let common = require('../controller/common')
let sendEmail = require("../controller/emialTranspot")
let query = require('../controller/mysql')
let data = require('../controller/data')
let crypto = require('../controller/crypto')

module.exports = {
    //注册 1.判断邮箱是否正确 2.判断密码是否存在 3.判断验证码是否存在 4.判断验证码是否正确 5.全部信息完整写进数据库
     async register(req,res) {
      let params = req.body
      //判断邮箱是否正确
       if(!params.email || !common.isEmail(params.email)){
          res.json({
            status:501,
            message:'邮箱格式不正确！'
          })
          return false;
       }
       //判断验证码是否存在
       if(!params.code){
         res.json({
           status:503,
           message:'请输入验证码'
         })
         return false;
       }
       //判断密码是否存在
       if(!params.psd){
        res.json({
          status:504,
          message:'请输入密码'
        })
        return false;
       }
       let isCode = await data.isCode(params.email,params.code)
       if(!isCode){
        res.json({
          status:506,
          message:'验证码错误！'
        })
        return false;
       }
       //将信息添加进数据库中
       let regsit = data.register([params.email,crypto.md5(params.psd), 1])
       if(regsit){
        res.json({
          status:200,
          message:'信息注册成功。'
        })
       }else{
        res.json({
          status:507,
          message:'服务器错误！！！'
        })
       }


    },
    // 获取验证码，1判断用户是否注册，2未注册返回验证码，已经注册返回提示
    async getCode(req,res) {
      let email = url.parse(req.url,true).query.email
      let isregsiter = await data.isRegister(email)
       if(!isregsiter){
        res.json({
          status:505,
          message:'用户已注册。'
        })
        return false;
       } else {
          if(common.isEmail(email)){
            //获取验证码 发送到邮箱
            let code = Math.round(Math.random()*8999 + 1000)
            let html = `<h1 strle="color:green">你的招聘网站邮箱验证码为:${code}<h1>`
            sendEmail(email,'注册验证码',html,function(error,response){
              
            if(error){
                res.json({
                  status: 510,
                  message: '邮箱发送失败'
                })
            } else {
              let sql = 'insert into verify(emial,code) values(?)'
              query(sql,[[email,code]]).then((result) =>{
                  res.json({
                    status:200,
                    data: code,
                    message:'验证码发送成功。'
                  })
              }).catch(function(error){
                  res.json({
                    data:502,
                    message:'验证码发送失败'
                  })
              })

            }
            })
        } else {
          res.json({
            data:501,
            message:'邮箱格式不正确'
          })
        }
       }
       
    }
}
