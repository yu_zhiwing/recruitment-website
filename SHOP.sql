/*
SQLyog Enterprise v12.09 (64 bit)
MySQL - 8.0.12 : Database - shop
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`shop` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `shop`;

/*Table structure for table `t_diqu` */

DROP TABLE IF EXISTS `t_diqu`;

CREATE TABLE `t_diqu` (
  `id` int(11) NOT NULL COMMENT '地区编号',
  `pid` int(11) DEFAULT NULL COMMENT '夫id',
  `name` varchar(50) DEFAULT NULL COMMENT '地区名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_diqu` */

/*Table structure for table `t_hangye` */

DROP TABLE IF EXISTS `t_hangye`;

CREATE TABLE `t_hangye` (
  `id` int(11) NOT NULL COMMENT '行业编号',
  `pid` int(11) DEFAULT NULL COMMENT '父id',
  `nmae` varchar(20) DEFAULT NULL COMMENT '行业名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_hangye` */

/*Table structure for table `t_jianli` */

DROP TABLE IF EXISTS `t_jianli`;

CREATE TABLE `t_jianli` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL COMMENT '姓名',
  `sex` char(1) DEFAULT NULL COMMENT '性别 1男2女',
  `birthday` date DEFAULT NULL COMMENT '出生日期',
  `huuy` varchar(10) DEFAULT NULL COMMENT '婚姻状态',
  `xuex` varchar(30) DEFAULT NULL COMMENT '毕业学校',
  `zhuany` varchar(30) DEFAULT NULL COMMENT '所学专业',
  `xuel` varchar(10) DEFAULT NULL COMMENT '最高学历',
  `gongzjy` varchar(700) DEFAULT NULL COMMENT '工作经验',
  `xiabjd` varchar(20) DEFAULT NULL COMMENT '现居地',
  `gangw` varchar(60) DEFAULT NULL COMMENT '求职岗位',
  `gangwlb` varchar(60) DEFAULT NULL COMMENT '岗位类别',
  `jishtch` varchar(100) DEFAULT NULL COMMENT '技术特长',
  `gongzjl` varchar(600) DEFAULT NULL COMMENT '工作经历',
  `gerjs` varchar(300) DEFAULT NULL COMMENT '个人介绍',
  `tel` char(11) DEFAULT NULL COMMENT '手机号',
  `email` varchar(30) DEFAULT NULL COMMENT '邮箱',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_jianli` */

/*Table structure for table `t_zhiwi` */

DROP TABLE IF EXISTS `t_zhiwi`;

CREATE TABLE `t_zhiwi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zhiw` varchar(20) DEFAULT NULL COMMENT '招聘职位',
  `leib` varchar(30) DEFAULT NULL COMMENT '职位类别',
  `dip` varchar(30) DEFAULT NULL COMMENT '工作地区地址',
  `rensh` decimal(10,0) DEFAULT NULL COMMENT '招聘人数',
  `yex` varchar(30) DEFAULT NULL COMMENT '月薪',
  `jiessh` varchar(200) DEFAULT NULL COMMENT '岗位介绍',
  `xuel` varchar(10) DEFAULT NULL COMMENT '学历',
  `jingy` varchar(20) DEFAULT NULL COMMENT '工作经验',
  `sex` char(1) DEFAULT NULL COMMENT '性别1男2女3不限制',
  `minage` int(5) DEFAULT NULL COMMENT '最小年龄',
  `maxage` int(5) DEFAULT NULL COMMENT '最顶年龄',
  `tel` char(11) DEFAULT NULL COMMENT '联系电话',
  `emial` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `createTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_zhiwi` */

/*Table structure for table `t_zhwjl` */

DROP TABLE IF EXISTS `t_zhwjl`;

CREATE TABLE `t_zhwjl` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `zhwid` int(11) DEFAULT NULL COMMENT '职位id',
  `jlid` int(11) DEFAULT NULL COMMENT '简历id',
  `data` timestamp NULL DEFAULT NULL COMMENT '时间',
  PRIMARY KEY (`id`),
  KEY `zhwid` (`zhwid`),
  KEY `jlid` (`jlid`),
  CONSTRAINT `t_zhwjl_ibfk_1` FOREIGN KEY (`zhwid`) REFERENCES `t_zhiwi` (`id`),
  CONSTRAINT `t_zhwjl_ibfk_2` FOREIGN KEY (`jlid`) REFERENCES `t_jianli` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_zhwjl` */

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emial` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '邮箱',
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户昵称',
  `password` varchar(50) DEFAULT NULL,
  `copename` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '公司昵称',
  `copenum` int(10) DEFAULT NULL COMMENT '公司人数',
  `copeaddres` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '公司详细地址',
  `copenature` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '公司性质',
  `copecapital` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '公司注册资金',
  `copeprofile` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '公司简介',
  `copecontacts` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '公司联系人',
  `copephone` char(11) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '联系人号码',
  `copeIncor` date DEFAULT NULL COMMENT '公司成立日期',
  `qq` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '联系人qq',
  `web` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '公司网站',
  `createTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '账号注册时间',
  `userid` int(10) DEFAULT NULL COMMENT '0 求职. 1招聘 3超级管理员',
  `status` int(11) DEFAULT NULL COMMENT '0 表示账号失效. 1表示正常',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

/*Data for the table `user` */

insert  into `user`(`id`,`emial`,`username`,`password`,`copename`,`copenum`,`copeaddres`,`copenature`,`copecapital`,`copeprofile`,`copecontacts`,`copephone`,`copeIncor`,`qq`,`web`,`createTime`,`userid`,`status`) values (9,'2570304823@qq.com',NULL,'56f59d0d14d605ed53478ff6800324a3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021-05-30 12:02:33',NULL,1),(10,'257030482@qq.com',NULL,'56f59d0d14d605ed53478ff6800324a3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021-06-04 23:45:35',NULL,1),(11,'25703048@qq.com',NULL,'56f59d0d14d605ed53478ff6800324a3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021-06-04 23:48:19',NULL,1),(12,'2570304@qq.com',NULL,'56f59d0d14d605ed53478ff6800324a3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021-06-04 23:49:09',NULL,1),(13,'570304823@qq.com',NULL,'56f59d0d14d605ed53478ff6800324a3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021-06-04 23:51:01',NULL,1);

/*Table structure for table `verify` */

DROP TABLE IF EXISTS `verify`;

CREATE TABLE `verify` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emial` varchar(50) DEFAULT NULL,
  `code` varchar(6) DEFAULT NULL,
  `createTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

/*Data for the table `verify` */

insert  into `verify`(`id`,`emial`,`code`,`createTime`) values (1,'2570304823@qq.com','5494','2021-05-29 14:31:25'),(2,'2570304823@qq.com','1374','2021-05-29 15:43:54'),(3,'3083695002@qq.com','2614','2021-05-29 15:43:55'),(4,'3083695002@qq.com','9262','2021-05-29 15:43:56'),(5,'1008611@qq.com','2670','2021-05-29 21:30:08'),(6,'1008611@qq.com','7763','2021-05-29 21:30:18'),(7,'1008611@qq.com','6373','2021-05-29 21:32:15'),(8,'1008611@qq.com','8127','2021-05-29 21:32:33'),(9,'1008611@qq.com','7437','2021-05-29 21:38:53'),(10,'25703043@qq.com','2768','2021-05-29 21:45:52'),(11,'25703043@qq.com','5188','2021-05-29 21:45:58'),(12,'2570303@qq.com','6648','2021-05-29 21:48:35'),(13,'25703023@qq.co','5902','2021-05-29 21:50:26'),(14,'2570823@qq.com','8291','2021-05-29 21:52:10'),(15,'2570304823@qq.com','7014','2021-05-29 21:55:07'),(16,'10086111@qq.com','2157','2021-05-29 22:29:11'),(17,'10086111@qq.com','6412','2021-05-29 22:31:31'),(18,'10086111@qq.com','2906','2021-05-29 22:34:32'),(19,'2570304823@qq.com','4377','2021-05-29 23:23:46'),(20,'3083695002@qq.com','7762','2021-05-29 23:30:50'),(21,'1008611@qq.com','2227','2021-05-30 00:13:15'),(22,'2570304823@qq.com','5823','2021-05-30 12:02:19'),(23,'2570304823@qq.com','6052','2021-06-04 23:20:37'),(24,'2570304823@qq.com','8904','2021-06-04 23:21:08'),(25,'2570304823@qq.com','9198','2021-06-04 23:28:49'),(26,'2570304823@qq.com','9547','2021-06-04 23:30:16'),(27,'2570304823@qq.com','2230','2021-06-04 23:30:39'),(28,'2570304823@qq.com','1595','2021-06-04 23:31:25'),(29,'257030482@qq.com','7700','2021-06-04 23:45:32'),(30,'25703048@qq.com','6690','2021-06-04 23:48:11'),(31,'2570304@qq.com','6145','2021-06-04 23:49:00'),(32,'570304823@qq.com','5193','2021-06-04 23:50:47'),(33,'2570304823@qq.com','6226','2021-06-04 23:53:56');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
