let nodemailer = require('nodemailer')

let nodemailerTransport = require('nodemailer-smtp-transport')

let config = require('../config')

//配置邮箱信息
nodemailerTransport = nodemailer.createTransport(nodemailerTransport({
    service: config.email.service,
    auth:{
        user: config.email.user,
        pass: config.email.pass
    },
    domains: [
        "qq.com"
    ],
    host: "smtp.qq.com",
    port: 465,
    secure: true
}))

/** 
 * @description 发送邮件
 * 
 * @param receipt {string} 收件人
 * @parem object {string} 发送主题
 * @param html {string} 邮件内容
 * 
*/

let sendEmail = function(receipt,subject,html,callback){
    nodemailerTransport.sendMail({
        from: config.email.user,
        to: receipt,
        subject,
        html
    },callback)
}


module.exports = sendEmail