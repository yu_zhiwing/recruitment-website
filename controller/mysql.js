let mysql = require('mysql')
//创建数据库连接

let config = require('../config')

let pool = mysql.createPool(config.mysql)

/**
 * @param : {string} sql 查询数据库语句
 * @param :{Array} data 查询数据库的语句
 * 
 */

 let query = function(sql,data){
     
     return new Promise(function(resolve,reject){
        //getconnection 获取连接 回调函数中返回
        //err 连接是否失败
        //connection 获取连接的信息
        pool.getConnection(function(err,conncetion){
            if(!err){
                // sql查询语句
                // data 查询过程中需要的数据
                // error 查询错误信息
                // result 查询结果
                conncetion.query(sql,data,function(error,result){
                    if(!error){
                        //将查询成功返回数据 存入resolve中
                        resolve(result)
                    }else{
                        reject(error)
                    }
                    conncetion.release()
                })
            }else{
                reject(err)
            }
        })

     })
 }


module.exports = query