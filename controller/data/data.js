/*
 * @Author: your name
 * @Date: 2020-05-16 22:52:00
 * @LastEditTime: 2021-05-30 00:51:33
 * @LastEditors: your name
 * @Description: In User Settings Edit
 * @FilePath: \EXAM\controller\data\data.js
 */
// 只要有数据库的操作都在这里进行
let query = require('../mysql')

module.exports = {
    //async 变成异步方法
    isRegister:async function(email){
        
     let data = await query('select * from user where emial = ?',email)
    console.log(data)
    if(data.length >0){
        // false 表示注册过
        return false
     }else{
         //表示没有注册过
         return true
     }
   },
   isCode: async function(email,code){
       let sql = 'select * from verify where emial = ? order by createTime desc'
        let data = await query(sql,email)
        if(data.length<=0) return false
        if(data[0].code == code){
            return true
        }
        return false
   },
   register:async function(data){
    let sql = 'insert into user(emial,password,status) values(?)'
    let result = await query(sql,[data]).catch(function(res){
        console.log(res)
    })
    if(result){
        return true
    }else{
        return false
    }
   },
   //验证用户登录信息
   login:async function(data){
        console.log(data)
       let sql = 'select * from user where emial=? and password=?'
       let result = await query(sql,data)
       if(result.length >0){
           return result[0]
       }else{
           return false
       }
   }
   
}